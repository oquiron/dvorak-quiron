# Presentation

[Documentation in Progress]

**Don't move your hands**

This is a Dvorak keyboard map for X, 
when an entire keyboard map with  the `CapsLock` key.

You don't need to move your hands because the most usable key are on the main key set. This include:

	Cursor Keys. Up, Down, Left, Right, AvPag, RePag, Home, End.
	Group Simbols: {} () [] 
	Common Simbols: @%*=?!+#¿~&_
	BackSpace
	
	
Example with QWERTY as reference:

	Caps	Lock + j			Left
	CapsLock + l			Right
	CapsLock + i			Up
	CapsLock + k			Down
	CapsLock + p			RePag
	CapsLock + u			Home
	...
	
	CapsLock + t			{
	CapsLock + y			}
	CapsLock + g			(
	CapsLock + d			)
	CapsLock + b			[
	CapsLock + n			]
	
	...
	
	CapsLock + a			@
	CapsLock + s			%
	CapsLock + d			*
	CapsLock + f			=
	CapsLock + c			!
	...







	

	
	
	

	
	